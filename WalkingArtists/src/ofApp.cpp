#include "ofApp.h"
#include "PlayerNodeReceiveNotifier.h"
#include <algorithm>

std::vector<shared_ptr<ofBaseApp>> ofApp::ofBehaviors;

template<typename Functor>
void ofApp::BehaviorLoop(Functor functor)
{
    for(shared_ptr<ofBaseApp> app : ofBehaviors)
    {
        functor(app.get());
    }
}

ofApp::~ofApp()
{
    ofBehaviors.clear();
}

//--------------------------------------------------------------
void ofApp::setup(){
    ofEnableAntiAliasing();
    BehaviorLoop([](ofBaseApp* app){ app->setup(); });
}

//--------------------------------------------------------------
void ofApp::update(){
    BehaviorLoop([](ofBaseApp* app){ app->update(); });
}

//--------------------------------------------------------------
void ofApp::draw(){
    BehaviorLoop([](ofBaseApp* app){ app->draw(); });
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    BehaviorLoop([=](ofBaseApp* app){ app->keyPressed(key); });
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
    BehaviorLoop([=](ofBaseApp* app){ app->keyReleased(key); });
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
    BehaviorLoop([=](ofBaseApp* app){ app->mouseMoved(x,y); });
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
    BehaviorLoop([=](ofBaseApp* app){ app->mouseDragged(x,y,button); });
    
    //test struct
    string id = "";
    switch (button)
    {
    case OF_MOUSE_BUTTON_LEFT:
        id = "left";
        break;
    case OF_MOUSE_BUTTON_RIGHT:
        id = "right";
        break;
    case OF_MOUSE_BUTTON_MIDDLE:
        id = "middle";
        break;
    default:
        break;
    }
    
    auto node = new PlayerNode(id);
    node->AddPlayerPosition(NodePosition(x, y, ofGetSystemTimeMicros()));
    PlayerNodeReceiveNotifier::getInstance()->notificationNewNode(node);
    delete node;
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    BehaviorLoop([=](ofBaseApp* app){ app->mousePressed(x,y,button); });
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
    BehaviorLoop([=](ofBaseApp* app){ app->mouseReleased(x,y,button); });
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){
    BehaviorLoop([=](ofBaseApp* app){ app->mouseEntered(x,y); });
}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){
    BehaviorLoop([=](ofBaseApp* app){ app->mouseExited(x,y); });
}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    BehaviorLoop([=](ofBaseApp* app){ app->windowResized(w,h); });
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){
    BehaviorLoop([=](ofBaseApp* app){ app->gotMessage(msg); });
}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 
    BehaviorLoop([=](ofBaseApp* app){ app->dragEvent(dragInfo); });
}

void ofApp::AddBehavior(ofBaseApp* behavior)
{
    ofBehaviors.push_back(shared_ptr<ofBaseApp>(behavior));
}

void ofApp::RemoveBehavior(ofBaseApp* behavior)
{
    auto it = std::find_if(std::begin(ofBehaviors), std::end(ofBehaviors), [=](shared_ptr<ofBaseApp> _behavior)->bool{
        return _behavior.get() == behavior;
    });
    ofBehaviors.erase(it);
}

