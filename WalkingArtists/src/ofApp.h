#pragma once

#include "ofMain.h"
#include <vector>

class ofApp : public ofBaseApp{

    private:
    static std::vector<shared_ptr<ofBaseApp>> ofBehaviors;
    
    template<typename Functor>
    void BehaviorLoop(Functor functor);
    
	public:
    
    ~ofApp();
    
    //base behavior
		void setup();
		void update();
		void draw();
    
    //input behavior
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
    
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    static void AddBehavior(ofBaseApp* behavior);
    static void RemoveBehavior(ofBaseApp* behavior);
};

