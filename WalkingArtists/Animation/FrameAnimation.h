#ifndef FrameAnimation_H
#define FrameAnimation_H

#include "ofMain.h"
#include <vector>

using namespace std;

class FrameAnimation
{
private:
    vector<ofImage> _img;
    vector<ofTexture> _tex;
    const int _frameCount;
    bool _isLoop;
    float _aniDuration;
    bool isPlaying;

    int currentFrame;
    float _aniTimer;
    ofPoint _position;

public:
    FrameAnimation(const char* fileNameFormat, const int frameCount, bool isLoop,const float duration);
    virtual ~FrameAnimation();

    void SetLoop(bool isLoop);
    void SetPosition(const ofPoint& pos);

    void Play();
    void Stop();
    void Pause();

    void Draw();
    void Update();
};

#endif //FrameAnimation_H