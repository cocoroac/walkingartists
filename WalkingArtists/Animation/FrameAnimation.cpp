#include "FrameAnimation.h"

FrameAnimation::FrameAnimation(const char* fileNameFormat, const int frameCount, bool isLoop, const float duration)
    :_frameCount(frameCount)
    ,_isLoop(isLoop)
    ,_aniDuration(duration)
{
    _img.resize(frameCount);
    for (int i = 0; i < frameCount; ++i)
    {
        char fileName[255];
        sprintf(fileName, fileNameFormat, i);
        _img[i].load(fileName);
    }

    currentFrame = 0;
}

FrameAnimation::~FrameAnimation()
{

}

void FrameAnimation::SetLoop(bool isLoop)
{
    _isLoop = isLoop;
}

void FrameAnimation::SetPosition(const ofPoint& pos)
{
    _position = pos - ofPoint(_img[currentFrame].getWidth() / 2, _img[currentFrame].getHeight() / 2);
}

void FrameAnimation::Play()
{
    isPlaying = true;
}

void FrameAnimation::Stop()
{
    isPlaying = false;
    currentFrame = 0;
}

void FrameAnimation::Pause()
{
    isPlaying = false;
}

void FrameAnimation::Draw()
{
    _img[currentFrame].draw(_position);
}

void FrameAnimation::Update()
{
    if (isPlaying)
    {
        _aniTimer += ofGetLastFrameTime()/_aniDuration;
        if (_aniTimer > _aniDuration)
            _aniTimer = 0;
        currentFrame = ofLerp(0, _frameCount-1, _aniTimer);
        currentFrame = currentFrame%_frameCount;
    }
    _img[currentFrame].update();
}
