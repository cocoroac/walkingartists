#include "PlayerNodeReceiveNotifier.h"
#include "IPlayerNodeReciver.h"

PlayerNodeReceiveNotifier* g_instance = nullptr;

PlayerNodeReceiveNotifier::PlayerNodeReceiveNotifier()
{

}

PlayerNodeReceiveNotifier::~PlayerNodeReceiveNotifier()
{

}

PlayerNodeReceiveNotifier* PlayerNodeReceiveNotifier::getInstance()
{
    if (g_instance == nullptr)
        g_instance = new PlayerNodeReceiveNotifier();

    return g_instance;
}

void PlayerNodeReceiveNotifier::registerReceiver(IPlayerNodeReciver* recevier)
{
    receivers.push_back(recevier);
}

void PlayerNodeReceiveNotifier::unregisterReceiver(IPlayerNodeReciver* receiver)
{
    auto it = find(receivers.begin(), receivers.end(), receiver);
    if(it != receivers.end())
        receivers.erase(it);
}

void PlayerNodeReceiveNotifier::notificationNewNode(const PlayerNode * const node)
{
    for (IPlayerNodeReciver* receiver : receivers)
    {
        if(receiver != nullptr)
            receiver->ReciveNode(node);
    }
}

void PlayerNodeReceiveNotifier::notificationSignalOut(const PlayerID id)
{

    for (IPlayerNodeReciver* receiver : receivers)
    {
        if(receiver != nullptr)
            receiver->SignalOut(id);
    }
}
