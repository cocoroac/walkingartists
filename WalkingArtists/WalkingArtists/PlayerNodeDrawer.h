//
//  PlayerNodeDrawer.hpp
//  WalkingArtists_OSX
//
//  Created by 종만 백 on 2016. 4. 21..
//
//

#ifndef PlayerNodeDrawer_hpp
#define PlayerNodeDrawer_hpp

#include <map>

#include "ofMain.h"
#include "PlayerNode.h"
#include "FrameAnimation.h"

using namespace std;

class PlayerNodeManager;

class PlayerNodeDrawer
{
private:
    PlayerNodeManager& _manager;
    map<PlayerID, shared_ptr<FrameAnimation>> _pointAni;

    ofFbo fbo;
    ofFbo back;
    ofShader shader;
    
    ofImage image;
    const int kWIDTH_DIV;
    const int kHEIGHT_DIV;
    
public:
    PlayerNodeDrawer(PlayerNodeManager& Manager);
    
    void Draw();
    void Update();

    void AddNode(const PlayerNode const* node);
    void SignalOut(const PlayerID id);
    
private:
    
};

#endif /* PlayerNodeDrawer_hpp */
