//
//  PlayerNodeManager.cpp
//   WalkingArtists
//
//  Created by 종만 백 on 2016. 4. 6..
//
//

#include <algorithm>
#include <assert.h>
#include <math.h>
#include "PlayerNodeManager.h"


PlayerNodeManager::PlayerNodeManager()
{
    
}

PlayerNodeList::iterator PlayerNodeManager::GetPlayerNodeIterator(const PlayerID& id)
{
    return std::find_if(std::begin(_nodeList), std::end(_nodeList), [=](PlayerNode* node) -> bool {
        return node->GetPlayerID() == id;
    });
}

void PlayerNodeManager::AddNode(const PlayerID& id,const NodePosition& node)
{
    auto it = GetPlayerNodeIterator(id);
    PlayerNode* p;
    if(it != std::end(_nodeList))
    {
        p = GetPlayerNode(id);
    }
    else
    {
        p = new PlayerNode(id);
        _nodeList.push_back(p);
    }
    p->AddPlayerPosition(node);
}

void PlayerNodeManager::CloseNode(const PlayerID& id)
{
    auto p = GetPlayerNode(id);
    if (p != nullptr)
        p->Close();
}

void PlayerNodeManager::RemoveNode(const PlayerID& id)
{
    auto it = GetPlayerNodeIterator(id);
    if(it != std::end(_nodeList))
        _nodeList.erase(it);
}
PlayerNode* PlayerNodeManager::GetPlayerNode(const PlayerID& id)
{
    auto it = GetPlayerNodeIterator(id);
    if(it != std::end(_nodeList))
        return *it;
    
    return nullptr;
}

PlayerNode* PlayerNodeManager::GetPlayerNode(const int i)
{
    assert(_nodeList.size() > i);
    return _nodeList.at(i);
}

size_t PlayerNodeManager::GetPlayerCount() const
{
    return _nodeList.size();
}

const PlayerNodeManager::EntropyMap& PlayerNodeManager::GetEntropyMap(const int width_div,const int height_div,const int width,const int height)
{
    _entropyMap.clear();
    
    //엔트로피 셀 초기화
    for(int x=0;x<width_div;++x)
    {
        for(int y=0;y<height;++y)
        {
            _entropyMap.insert(make_pair(make_pair(x, y), 0));
        }
    }
    
    //셀사이즈 구함.
    float width_cell_size = (float)width/(float)width_div;
    float height_cell_size = (float)height/(float)height_div;
    
    EntropyMap::iterator maxmum_entropy = _entropyMap.begin();
    
    for(PlayerNode* node : _nodeList)
    {
        //연속된 버택스가 연결된 선이 관통하는 셀을 구합니다.
        for(int posIndex=0;posIndex<node->GetPositionCount()-1;++posIndex)
        {
            NodePosition const& pos_1 = node->GetPosition(posIndex);
            NodePosition const& pos_2 = node->GetPosition(posIndex+1);
            //두 버택스를 포함하고 있는 셀을 구함.
            int cell_x_1 = pos_1._x/width_cell_size;
            int cell_y_1 = pos_1._y/height_cell_size;
            int cell_x_2 = pos_2._x/width_cell_size;
            int cell_y_2 = pos_2._y/height_cell_size;
            
            if (cell_x_1 > 0 && cell_y_1 > 0 && cell_x_2 > 0 && cell_y_2 > 0)
            {
                //두 셀이 같은 셀이라면 그냥 바로 처리
                if ((cell_x_1 & cell_x_2) && (cell_y_1 & cell_y_2))
                {
                    EntropyMap::iterator it = _entropyMap.find(make_pair(cell_x_1, cell_y_1));
                    it->second += 1;

                    //맥시멈 저장해서 나중에 엔트로피 평준화 할때 씀.
                    if (maxmum_entropy->second < it->second)
                    {
                        maxmum_entropy = it;
                    }
                }
                else
                {
                    //직선과 교차하는 선 구하기
                }
            }
        }
    }

    //최대값을 기반으로 평준화 시킨다.
    float maxmum_entropy_value = maxmum_entropy->second;
    for (int x = 0; x < width_div; ++x)
    {
        for (int y = 0; y < height; ++y)
        {
            _entropyMap[make_pair(x, y)] = _entropyMap[make_pair(x, y)] / maxmum_entropy_value;
        }
    }
    
    return _entropyMap;
}
