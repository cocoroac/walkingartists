//
//  IPlayerNodeReciver.h
//   WalkingArtists
//
//  Created by 종만 백 on 2016. 4. 6..
//
//

#ifndef IPlayerNodeReciver_h
#define IPlayerNodeReciver_h

#include "PlayerNode.h"
#include "PlayerNodeReceiveNotifier.h"

class IPlayerNodeReciver{
public:
    IPlayerNodeReciver()
    {
        PlayerNodeReceiveNotifier::getInstance()->registerReceiver(this);
    }

    virtual ~IPlayerNodeReciver()
    {
        PlayerNodeReceiveNotifier::getInstance()->unregisterReceiver(this);
    }
    //플레이어 위치 정보를 수신했을때 입니다.
    virtual void ReciveNode(const PlayerNode const* node) = 0;
    //플레이어의 신호를 잃었을때 입니다.
    virtual void SignalOut(const PlayerID id) = 0;
};

#endif /* IPlayerNodeReciver_h */
