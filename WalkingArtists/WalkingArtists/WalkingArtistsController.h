#ifndef WalkingArtistsController_H
#define WalkingArtistsController_H

#include "ofBaseApp.h"
#include "PlayerNodeManager.h"
#include "IPlayerNodeReciver.h"

#include "ofMain.h"

#include <map>
#include <memory>

using namespace std;

class PlayerNodeDrawer;

class WalkingArtistsController : public ofBaseApp, public IPlayerNodeReciver
{
private:
    unique_ptr<PlayerNodeManager> _player_node_manager;
    unique_ptr<PlayerNodeDrawer> _drawer;
    
public:
    WalkingArtistsController();
    virtual ~WalkingArtistsController();
    virtual void update() override;
    virtual void draw() override;


    virtual void ReciveNode(const PlayerNode *const node) override;
    virtual void SignalOut(const PlayerID id) override;

};

#endif // WalkingArtistsController_H
