//
//  PlayerNode.hpp
//   WalkingArtists
//
//  Created by 종만 백 on 2016. 4. 6..
//
//

#ifndef PlayerNode_hpp
#define PlayerNode_hpp

#include<iostream>
#include<string>
#include<Vector>
#include "WalkingArtistsPolyline.h"

using namespace std;

class PlayerNode
{
private:
    PlayerID _pID;
    PlayerPositionList _positionList;

    WalkingArtistsPolyline _line;
    bool bClose;

private:
    bool isClose() const;

public:
    PlayerNode(const PlayerID pId);
    virtual ~PlayerNode();
    
    PlayerID const& GetPlayerID() const&;
    
    NodePosition const& GetLastPosition() const&;
    void AddPlayerPosition(const NodePosition& position);
    
    NodePosition const& GetPosition(int index) const;
    size_t GetPositionCount() const;

    void Draw();
    void Update();

    //더이상 선이 그어지지 않습니다.
    void Close();
};

#endif /* PlayerNode_hpp */
