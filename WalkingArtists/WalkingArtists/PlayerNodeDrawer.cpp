//
//  PlayerNodeDrawer.cpp
//  WalkingArtists_OSX
//
//  Created by 종만 백 on 2016. 4. 21..
//
//

#include "PlayerNodeDrawer.h"
#include "PlayerNodeManager.h"

PlayerNodeDrawer::PlayerNodeDrawer(PlayerNodeManager& Manager)
:_manager(Manager)
,kWIDTH_DIV(50)
,kHEIGHT_DIV(50)
{
    printf("%d %d", ofGetWidth(), ofGetHeight());
    fbo.allocate(ofGetWidth(),ofGetHeight());
    back.allocate(ofGetWidth(), ofGetHeight());
    
#ifdef TARGET_OPENGLES
    shader.load("shadersES2/shader");
#else
    if(ofIsGLProgrammableRenderer()){
        shader.load("shadersGL3/shader");
    }else{
        shader.load("shadersGL2/shader");
    }
#endif
}

void PlayerNodeDrawer::Draw()
{
    ofClear(255, 255, 255,255);
    fbo.begin();
        ofClear(255,255,255);
        for(int i=0;i<_manager.GetPlayerCount();++i)
        {
            _manager.GetPlayerNode(i)->Draw();
        }
    fbo.end();
    
    ofTexture& texture = fbo.getTexture();
    const ofTexture* mask = texture.getAlphaMask();
    
    shader.begin();
        shader.setUniformTexture("imageMask", texture, 1);
        back.draw(0,0);
    shader.end();

    for (pair<PlayerID, shared_ptr<FrameAnimation>> pointAni : _pointAni)
    {
        pointAni.second->Draw();
    }
}

void PlayerNodeDrawer::Update()
{
    for(int i=0;i<_manager.GetPlayerCount();++i)
    {
        _manager.GetPlayerNode(i)->Update();
    }
    
    //make entropy texture
    const PlayerNodeManager::EntropyMap& entropy_map = _manager.GetEntropyMap(kWIDTH_DIV, kHEIGHT_DIV, ofGetWidth(), ofGetHeight());
    
    const float cell_width_size = (float)ofGetWidth()/kWIDTH_DIV;
    const float cell_height_size = (float)ofGetHeight()/kHEIGHT_DIV;
    
    back.begin();
    for(int i=0;i<kWIDTH_DIV;++i)
    {
        for(int j=0;j<kHEIGHT_DIV;++j)
        {
            float entropy = entropy_map.at(PlayerNodeManager::EntropyMapKey(i,j));
            ofFill();
            ofSetColor((int)(55.0f * entropy + 200), 200, (int)(55.0f * (1-entropy)+200));
            ofDrawRectangle(cell_width_size*i, cell_height_size*j, cell_width_size, cell_height_size);
        }
    }
    back.end();

    for (pair<PlayerID, shared_ptr<FrameAnimation>> pointAni : _pointAni)
    {
        pointAni.second->Update();
    }
}

void PlayerNodeDrawer::AddNode(const PlayerNode const* node)
{
    map<PlayerID, shared_ptr<FrameAnimation>>::iterator it = _pointAni.find(node->GetPlayerID());
    if (it == _pointAni.end())
    {
        pair<PlayerID, shared_ptr<FrameAnimation>> newAni = make_pair(node->GetPlayerID(), make_shared<FrameAnimation>("image/light/Final_%05d.png", 90, true, 2));
        newAni.second->Play();
        _pointAni.insert(newAni);
    }
    else
    {
        it->second->SetPosition(ofPoint(node->GetLastPosition()._x, node->GetLastPosition()._y));
    }
}

void PlayerNodeDrawer::SignalOut(const PlayerID id)
{
    _pointAni.erase(id);
}
