#ifndef PlayerNodeReceiveNotifier_H
#define PlayerNodeReceiveNotifier_H

#include <vector>
#include "PlayerNode.h"

using namespace std;

class IPlayerNodeReciver;

class PlayerNodeReceiveNotifier
{
private:
    PlayerNodeReceiveNotifier();    
    vector<IPlayerNodeReciver*> receivers;

public:
    ~PlayerNodeReceiveNotifier();
    static PlayerNodeReceiveNotifier* getInstance();

    void registerReceiver(IPlayerNodeReciver* recevier);
    void unregisterReceiver(IPlayerNodeReciver* receiver);
    
    void notificationNewNode(const PlayerNode * const node);
    void notificationSignalOut(const PlayerID id);

};

#endif //PlayerNodeReceiveNotifier_H