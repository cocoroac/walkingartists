#include "WalkingArtistsController.h"
#include "PlayerNodeDrawer.h"
#include "ofApp.h"

WalkingArtistsController::WalkingArtistsController()
{
    _player_node_manager = make_unique<PlayerNodeManager>();
    _drawer = make_unique<PlayerNodeDrawer>(*(_player_node_manager.get()));
}

WalkingArtistsController::~WalkingArtistsController()
{

}

void WalkingArtistsController::update()
{
    _drawer->Update();
}

void WalkingArtistsController::draw()
{
    _drawer->Draw();
}

void WalkingArtistsController::ReciveNode(const PlayerNode *const node)
{
    _player_node_manager->AddNode(node->GetPlayerID(), node->GetLastPosition());
    _drawer->AddNode(node);
}

void WalkingArtistsController::SignalOut(const PlayerID id)
{
    _player_node_manager->CloseNode(id);
    _drawer->SignalOut(id);
}
