#ifndef WalkingArtistsPolyline_h
#define WalkingArtistsPolyline_h

#include "PlayerNodeData.h"

class WalkingArtistsPolyline
{

private:
    PlayerPositionList _list;

    double _roamingTime;
    double kROAMING_START_TRESHOLD_SEC;
    const int kROAMING_RADIUS;

    NodePosition _lastPosition;
    const int kDefaultLineWidth;
    const int kMaxLineWidth;
    float _maxSpeed;
    float _minSpeed;

private:
    void roamingLine();
    void AddRoamingTime(double addTime);
    double GetRoamingTime() const;
    double GetRoamingProcessivity();
    void ClearRoamingTime();
    bool isStartRoaming() const;

public:
    WalkingArtistsPolyline();
    virtual ~WalkingArtistsPolyline();

    void clear();
    void addVertex(const NodePosition& p);

    void draw();
    void update();

    void caluAvgSpeed();
    float caluSpeed(NodePosition &p2, NodePosition &p1);

};

#endif //WalkingArtistsPolyline_h