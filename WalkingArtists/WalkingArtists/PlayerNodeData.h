#include <vector>

using namespace std;

typedef string PlayerID;
typedef struct _Position
{
    float _x;
    float _y;
    //위치가 추가된 시간, 프로그램 시작 후 부터

    uint64_t  _addedTimeOffsetFromStart;

    bool isNull() { return _x == 0 && _y == 0 && _addedTimeOffsetFromStart == -1; }

    _Position():_Position(0,0,-1)
    {
    }
    _Position(int x, int y, uint64_t addedTimeOffsetFromStart)
    {
        _x = x;
        _y = y;
        _addedTimeOffsetFromStart = addedTimeOffsetFromStart;
    }

} NodePosition;
typedef vector<NodePosition> PlayerPositionList;