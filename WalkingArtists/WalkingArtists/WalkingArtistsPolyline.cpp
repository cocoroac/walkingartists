#include "WalkingArtistsPolyline.h"
#include "ofAppRunner.h"
#include "ofMain.h"

WalkingArtistsPolyline::WalkingArtistsPolyline()
:_roamingTime(0)
, kROAMING_RADIUS(10)
, kROAMING_START_TRESHOLD_SEC(5.0)
, kDefaultLineWidth(3)
, kMaxLineWidth(20)
, _maxSpeed(0)
, _minSpeed(0)
{

}

WalkingArtistsPolyline::~WalkingArtistsPolyline()
{

}

void WalkingArtistsPolyline::roamingLine()
{
    AddRoamingTime(ofGetLastFrameTime());
    if (isStartRoaming())
    {
        float currentDistance = min<float>(50.0, (ofLerp(0, kROAMING_RADIUS, GetRoamingProcessivity())));

        float angle = ofDegToRad(ofLerp(0, 360.0, GetRoamingProcessivity()));

        float xOffset = cos(angle) * currentDistance + (ofNoise(currentDistance) * 20);
        float yOffset = sin(angle) * currentDistance + (ofNoise(currentDistance) * 20);

        
        _list.push_back(NodePosition(_lastPosition._x + xOffset, _lastPosition._y + yOffset,0));
    }
}

void WalkingArtistsPolyline::AddRoamingTime(double addTime)
{
    _roamingTime += addTime;
}

double WalkingArtistsPolyline::GetRoamingTime() const
{
    return _roamingTime;
}

double WalkingArtistsPolyline::GetRoamingProcessivity()
{
    return _roamingTime - kROAMING_START_TRESHOLD_SEC;
}

void WalkingArtistsPolyline::ClearRoamingTime()
{
    _roamingTime = 0;
}

bool WalkingArtistsPolyline::isStartRoaming() const
{
    return (GetRoamingTime() > kROAMING_START_TRESHOLD_SEC) & (_list.size() > 0);
}

void WalkingArtistsPolyline::clear()
{
    _list.clear();
}

void WalkingArtistsPolyline::addVertex(const NodePosition& p)
{
    //로밍중이었다면 마지막지점으로 버택스를 옮긴다.
    if (isStartRoaming())
    {
        _list .push_back(_lastPosition);
    }

    _list.push_back(p);
    _lastPosition = p;

    ClearRoamingTime();
}

void WalkingArtistsPolyline::draw()
{
    //TODO: line drawing
    for (int i = 0; i < _list.size()-1; ++i)
    {
        NodePosition& p1 = _list[i];
        NodePosition& p2 = _list[i + 1];

        //RoamingLine
        if (p2._addedTimeOffsetFromStart == 0)
        {
            ofSetLineWidth(kDefaultLineWidth);
        }
        else
        {
            float width = ofNormalize(caluSpeed(p2, p1), _minSpeed, _maxSpeed);
            ofSetLineWidth(max(1.0f,kMaxLineWidth*width)); 
        }
        //선 긋기.
        ofDrawLine(p1._x,p1._y, p2._x,p2._y);
    }
}

void WalkingArtistsPolyline::update()
{
    caluAvgSpeed();
    roamingLine();
}

void WalkingArtistsPolyline::caluAvgSpeed()
{
    for (int i = 0; i < _list.size()-1; ++i)
    {
        NodePosition& p1 = _list[i];
        NodePosition& p2 = _list[i + 1];

        if (p2._addedTimeOffsetFromStart - p1._addedTimeOffsetFromStart == 0)
            continue;

        if (p1._addedTimeOffsetFromStart != 0 && p2._addedTimeOffsetFromStart != 0)
        {
            float speed = caluSpeed(p2, p1);

            if (speed > _maxSpeed)
                _maxSpeed = speed;
            else if (speed < _minSpeed)
                _minSpeed = speed;
        }
    }
}

float WalkingArtistsPolyline::caluSpeed(NodePosition &p2, NodePosition &p1)
{
    const float distance = ofPoint(p1._x, p1._y).distance(ofPoint(p2._x, p2._y));
    const float speed = distance*10000 / (p2._addedTimeOffsetFromStart - p1._addedTimeOffsetFromStart);
    return speed;
}
