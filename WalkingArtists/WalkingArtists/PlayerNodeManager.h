//
//  PlayerNodeManager.hpp
//   WalkingArtists
//
//  Created by 종만 백 on 2016. 4. 6..
//
//

#ifndef PlayerNodeManager_hpp
#define PlayerNodeManager_hpp

#include "PlayerNode.h"

#include <memory>
#include <vector>
#include <map>

using namespace std;

typedef vector<PlayerNode*> PlayerNodeList;

class PlayerNodeManager
{
public:
    typedef pair<int, int> EntropyMapKey;
    typedef map<EntropyMapKey,float> EntropyMap;
    
private:
    PlayerNodeList _nodeList;
    PlayerNodeList::iterator GetPlayerNodeIterator(const PlayerID& id);
    EntropyMap _entropyMap;
    
public:
    PlayerNodeManager();
    
    void AddNode(const PlayerID& id,const NodePosition& node);
    void CloseNode(const PlayerID& id);
    
    void RemoveNode(const PlayerID& id);
    
    PlayerNode* GetPlayerNode(const PlayerID& id);
    PlayerNode* GetPlayerNode(const int i);
    
    size_t GetPlayerCount() const;
    
    //노드의 포지션별 혼잡도를 얻는다.
    //@param width_div : 가로크기를 몇 구획 나눌지에 해당하는 값
    //@param height_div : 세로크기를 몇 구획 나눌지에 해당하는 값.
    //@param width : 가로크기
    //@param height : 세로크기
    //@return : 2차원 배열로된 혼잡도를 얻는다. 각 셀당 0~1f까지의 값이 들어가며, 배열크기는 width_div * height_div로 돌려준다.
    const EntropyMap& GetEntropyMap(const int width_div,const int height_div,const int width,const int height);
};

#endif /* PlayerNodeManager_hpp */
