//
//  PlayerNode.cpp
//   WalkingArtists
//
//  Created by 종만 백 on 2016. 4. 6..
//
//

#include <algorithm>
#include "PlayerNode.h"
#include "ofGraphics.h"
#include "ofAppRunner.h"
#include <math.h>

PlayerNode::PlayerNode(const PlayerID pId)
:_pID(pId)
,bClose(false)
{
    
}

PlayerNode::~PlayerNode()
{
}

PlayerID const& PlayerNode::GetPlayerID() const&
{
    return _pID;
}
    
NodePosition const& PlayerNode::GetLastPosition() const&
{
    assert(_positionList.size() > 0);
    return _positionList.back();
    
}

void PlayerNode::AddPlayerPosition(const NodePosition& position)
{
    if (isClose())
        return;

    _positionList.push_back(position);
    _line.addVertex(position);
}
    
NodePosition const& PlayerNode::GetPosition(int index) const
{
    if(index < GetPositionCount())
    {
        return _positionList.at(index);
    }
}

size_t PlayerNode::GetPositionCount() const
{
    return _positionList.size();
}

void PlayerNode::Draw()
{
    _line.draw();
}

void PlayerNode::Update()
{
    _line.update();
}

bool PlayerNode::isClose() const
{
    return bClose;
}

void PlayerNode::Close()
{
    bClose = true;
}
